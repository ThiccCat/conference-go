import json
import pika
import django
import os
import sys
from django.core.mail import send_mail

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_rejection(ch, method, properties, body):
    print("ab")
    bodies = body.decode()
    b = json.loads(bodies)
    send_mail(
        "Your presentation has been rejected",
        f"{b['presenter_name']}, we're happy to tell you that you suck ass",
        "admin@conference.go",
        [f"{b['presenter_email']}"],
        fail_silently=False,
    )
    print("bb")


def process_approval(ch, method, properties, body):
    print("ab")
    bodies = body.decode()
    b = json.loads(bodies)
    send_mail(
        "Your presentation has been accepteed",
        f"{b['presenter_name']}, we're happy to tell you that your presentation {b['title']} has been accepted",
        "admin@conference.go",
        [f"{b['presenter_email']}"],
        fail_silently=False,
    )
    print("bb")


parameters = pika.ConnectionParameters(host="rabbitmq")
print("a")
connection = pika.BlockingConnection(parameters)
print("b")
channel = connection.channel()
print("c")
channel.queue_declare(queue="presentation_approvals")
print("d")
channel.basic_consume(
    queue="presentation_approvals",
    on_message_callback=process_approval,
    auto_ack=True,
)
channel.basic_consume(
    queue="presentation_rejections",
    on_message_callback=process_rejection,
    auto_ack=True,
)
print("e")
channel.start_consuming()
print("f")
